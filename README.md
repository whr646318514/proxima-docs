# Proxima.One
### Decentralized Data Layer for Web 3.0

### A blockchain index that features consensus-less writes and reads and can be integrated by any chain

With the main features being filtering, sorting, and Elasticsearch-esque text search.
Our product has a current Testnet for its registration, with an API authentication mechanism on its way. People will be able to check out the registry via the registry explorer. 

[Read more](https://github.com/proxima-one/docs/blob/master/Introduction.md)

Check us out!
[proxima.one](https://www.proxima.one/)


## Important Links
- [Introduction](https://github.com/proxima-one/docs/blob/master/Introduction.md)
- [Proxima Exporer](https://github.com/proxima-one/proxima_explorer)
 
